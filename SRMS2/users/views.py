from django.shortcuts import render
from .forms import *
from .decorator import *
from django.urls import reverse_lazy
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from school.models import Information, TeacherCourse
from .models import User
from django.contrib import auth
from .forms import CustomUserCreationForm, CustomUserChangeForm
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils import timezone
# Create your views here.

def login(request):
    if request.method == 'POST':
        user = auth.authenticate(user_id=request.POST['user_id'],password=request.POST['password'])
        if user is not None:
            auth.login(request, user)
            return redirect('home')
   
        else:
            return render(request,'registration/login.html',{'error':'Username or Password incorrect!'})
    else:
        return render(request,'registration/login.html')

def home(request):
    return render(request, "_base.html")

def student_list_view(request):
    students = User.objects.filter(role="student")
    return render(request, 'student_list.html', {'students' : students})

def teacher_list_view(request):
    teachers = User.objects.filter(role="teacher")
    return render(request, 'teacher_list.html', {'teachers' : teachers})

class CreateUserView(LoginRequiredMixin, CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('home')
    template_name = 'user_create.html'

class UserUpdateView(LoginRequiredMixin, UpdateView):
    model = User
    fields = ('first_name', 'middle_name', 'last_name', 'email', 'phone', 'role', 'address')
    success_url = reverse_lazy('home')
    template_name = 'user_update.html'

class UserDeleteView(LoginRequiredMixin, DeleteView):
    model = User
    success_url = reverse_lazy('home')
    template_name = 'user_delete.html'

class AssignCourseListView(LoginRequiredMixin, ListView):
    model = TeacherCourse
    template_name = 'assign_course_list.html'
    

class AssignCourseUpdateView(LoginRequiredMixin, UpdateView):
    model = TeacherCourse
    fields = ('__all__')
    success_url = reverse_lazy('assign_course_list')
    template_name = 'assign_course_edit.html'

class AssignCourseDeleteView(LoginRequiredMixin, DeleteView):
    model = TeacherCourse
    success_url = reverse_lazy('assign_course_list')
    template_name = 'assign_course_delete.html'


@login_required(login_url='login')
@officer_authentication
def admission(request):
    error = None
    if request.method == "POST":
        form = Admission(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            try:
                year = int(Information.objects.get(name='academic_year').value)
            except Information.DoesNotExist:
                year = timezone.now().year - 7
            
            post.year = year
            try:
                post.save()
                return redirect('admission')
            except Exception:
                error = "{} registered already in year {}".format(post.student, year) 
    else:
        form = Admission()
    return render(request, "admission.html", {"form": form, 'error': error})


@login_required(login_url='login')
@officer_authentication
def course_assign(request):
    error = None
    if request.method == "POST":
        form = CourseAssign(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            try:
                year = int(Information.objects.get(name='academic_year').value)
            except Information.DoesNotExist:
                year = timezone.now().year - 7
            
            post.year = year
            try:
                post.save()
                return redirect('course_assign')
            except Exception:
                already_assign = TeacherCourse.objects.get(course=post.course, year=post.year)
                error = "{} was already assigned for {} class in {}".format(already_assign.teacher, already_assign.course, already_assign.year) 
    else:
        form = CourseAssign()
    return render(request, "course_assign.html", {"form": form, 'error': error})

