from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('home/', views.home, name='home'),
    path('signin/', views.login, name='signin'),
    path('admission/', views.admission, name='admission'),
    path('course-assign/', views.course_assign, name='course_assign'),
    path('assign_course_list/', views.AssignCourseListView.as_view(), name='assign_course_list'),
    path('assign/course/edit/<int:pk>/', views.AssignCourseUpdateView.as_view(), name='assign_course_edit'),
    path('assign/course/delete/<int:pk>/', views.AssignCourseDeleteView.as_view(), name='assign_course_delete'),

    path('register/', views.CreateUserView.as_view(), name='register'),
    path('student_list/',views.student_list_view, name='students_list'),
    path('teachers_list/',views.teacher_list_view, name='teachers_list'),

    path('user/update/<int:pk>/', views.UserUpdateView.as_view(), name='user_update'),
    path('user/delete/<int:pk>/', views.UserDeleteView.as_view(), name='user_delete'),
]