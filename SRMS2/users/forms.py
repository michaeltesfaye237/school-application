from django import forms
from school.models import StudentSection, TeacherCourse
from .models import User
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth import get_user_model
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column

class CustomUserCreationForm(UserCreationForm):
    class Meta:
        model = get_user_model()
        fields = ('user_id', 'first_name', 'middle_name', 'last_name', 'email', 'phone', 'sex', 'photo','role', 'address', 'register_date')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column('user_id', css_class='form-group col-md-5 offset-sm-1 mb-0'),
                css_class='form-row'
            ),
            Column('username', css_class='form-group col-md-5 offset-sm-1 mb-0'),
            Row(
                Column('first_name', css_class='form-group col-md-3 offset-sm-1 mb-0'),
                Column('middle_name', css_class='form-group col-md-3 mb-0'),
                Column('last_name', css_class='form-group col-md-3 mb-0'),
                css_class='form-row'
            ),
            Column('email', css_class='form-group col-md-5 offset-sm-1 mb-0'),
            Column('phone', css_class='form-group col-md-5 offset-sm-1 mb-0'),
            Column('sex', css_class='form-group col-md-5 offset-sm-1 mb-0'),
            Column('address', css_class='form-group col-md-5 offset-sm-1 mb-0'),
            Column('role', css_class='form-group col-md-5 offset-sm-1 mb-0'),
            Column('photo', css_class='form-group col-md-5 offset-sm-1 mb-0'),
            Column('register_date', css_class='form-group col-md-5 offset-sm-1 mb-0'),
            Column('password1', css_class='form-group col-md-5 offset-sm-1 mb-0'),
            Column('password2', css_class='form-group col-md-5 offset-sm-1 mb-0'),
            Submit('submit', 'Register', css_class='offset-sm-1')
        )

class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = get_user_model()
        fields = ('user_id', 'first_name', 'middle_name', 'last_name', 'email', 'phone', 'photo', 'address',)

class Admission(forms.ModelForm):
    class Meta:
        model = StudentSection
        fields = ('student', 'section')

class CourseAssign(forms.ModelForm):
    class Meta:
        model = TeacherCourse
        fields = ('teacher', 'course')



