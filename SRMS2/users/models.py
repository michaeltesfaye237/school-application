from django.utils.safestring import mark_safe
from django.core.exceptions  import ValidationError
from django.core.validators import RegexValidator
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)

# Create your models here.

class UserManager(BaseUserManager):
    def create_user(self, user_id, first_name, middle_name,  last_name,  email, phone, sex, address, password, role):
 
        if not middle_name:
            raise ValueError('Users must have an middle name')
        
        if not first_name:
            raise ValueError('Users must have an first name')

        if not last_name:
            raise ValueError('Users must have an last name')

        if not user_id:
            raise ValueError('Users must have an ID')

        if not role:
            raise ValueError('Users must have a role')
        
        if not sex:
            raise ValueError('Users must have a sex')
    
  
        user = self.model(
            user_id = user_id,
            middle_name=middle_name,
            first_name=first_name,
            last_name=last_name,
            email=self.normalize_email(email),
            role=role,
            sex=sex,
            phone=phone,
            address=address,
            register_date=timezone.now().date(),
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, user_id, password):
        """
        Creates and saves a superuser with the given user_id, password ...
        """
        
        user = self.create_user(
            user_id = user_id,
            first_name=' ',
            middle_name=' ',
            last_name=' ',
            email=' ',
            password=password,
            role=' ',
            sex=' ',
            phone=' ',
            address=' ',
        )
        
        user.is_admin = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    user_id = models.CharField(max_length=200, unique=True)
    first_name = models.CharField(max_length=200)
    middle_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        blank=True,
    )
    phone_regex = RegexValidator(
        regex=r'^\+?1?\d{10,15}$',
        message='Phone number must be entered in the format : 09******** or +2519******** up to 15 digits allowed',
        )
        
    phone = models.CharField(validators=[phone_regex], max_length=15, blank=True)
    sex = models.CharField(max_length=200, choices=(('male', 'male'), ('female', 'female')))
    photo = models.ImageField(blank=True, upload_to='', default='null.png')
    role = models.CharField(max_length=200, choices=(('student', 'student'), ('officer', 'officer'), ('teacher', 'teacher')))
    address = models.CharField(max_length=200, blank=True)
    is_admin = models.BooleanField(default=False)
    register_date = models.DateField(default = timezone.now)
    objects = UserManager()

    USERNAME_FIELD = 'user_id'
    REQUIRED_FIELDS = []


    def image_tag(self):
        return mark_safe('<img src="%s" width="150" height="150"/>' % self.photo.url)

    image_tag.short_description = 'Photo'
    image_tag.allow_tags = True

    def __str__(self):
        # return self.username
        return self.first_name + ' ' + self.middle_name

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin
