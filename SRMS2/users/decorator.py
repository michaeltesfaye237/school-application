from django.core.exceptions import PermissionDenied
from users.models import User
from django.shortcuts import redirect

def teacher_authentication(function):
    def wrap(request, *args, **kwargs):
        if request.user.role == "teacher":
            return function(request, *args, **kwargs)
        else:
            return redirect('login')
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


def student_authentication(function):
    def wrap(request, *args, **kwargs):
        if request.user.role == "student":
            return function(request, *args, **kwargs)
        else:
            return redirect('login')
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap



def officer_authentication(function):
    def wrap(request, *args, **kwargs):
        if request.user.role == "officer":
            return function(request, *args, **kwargs)
        else:
            return redirect('login')
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap

