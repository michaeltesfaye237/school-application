# f = input('You\'re first name: ')
# first_name = f.capitalize()
# l = input('You\'re last name:')
# last_name = l.capitalize()
# print(f'Salut, {first_name} {last_name}')
from datetime import datetime, timedelta
# current_date = datetime.now()
# print(f'Today : {current_date}')

# one_day = timedelta(days=1)
# yesterday = current_date - one_days
# print(f'Yesterday : {yesterday}')
# birthday = input('Enter you\'re birthday (dd/mm/yyyy) :')
# birthday_date = datetime.strptime(birthday, '%d/%m/%Y')
# print(birthday_date) 


# x = 10
# y = 0
# try:
#     print(x/y)
# except ZeroDivisionError as e:  
#     print('Can\'t divide by zero')
# finally:
#     print('Division operation')

# from array import array
# age = array('d')
# age.append(97)
# age.append(98)
# age.append(99)
# print(age)

# employees = {'first' : 'Mike'}
# lst = []
# lst.append(employees)
# lst.append({
#     'second' : 'melaku'
# })

# start = datetime.now()
# print(start)
# for x in range(0,100):
#     print(x)

# end = datetime.now()
# print(end)
# print((end - start).seconds)

# pip install -r requirements.txt

# pip install virtualenv ->>globally install it
# python -m venv <folder_name>   ->>windows OS
# using virtual environments
# <folder_name>\Scripts\Activate.bat  ->>cmd.exe
# <folder_name>\Scripts\Activate.ps1  ->>Powershell.exe
# . ./<folder_name>/Scripts/activate  ->>bash shell


# #JSON - we can use a dictionary to create a JSON
# import json
# pucho = { 'first_name':'Mike', 'last_name':'Tesfaye', 'birth_place':'AA'}
# pucho['age'] = 23

# # convert dictionary into a JSON object
# pucho_json = json.dumps(pucho)
# print(pucho_json)

# import os 
# os_version = os.getenv('OS')
# print(os_version)
