# Generated by Django 2.2.5 on 2020-11-22 08:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('school', '0007_auto_20201122_1142'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='teachercourse',
            unique_together={('year', 'course')},
        ),
    ]
