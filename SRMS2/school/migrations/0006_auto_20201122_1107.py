# Generated by Django 2.2.5 on 2020-11-22 08:07

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('school', '0005_information'),
    ]

    operations = [
        migrations.AlterField(
            model_name='studentsection',
            name='year',
            field=models.PositiveIntegerField(default=2013, validators=[django.core.validators.MinValueValidator(2010), django.core.validators.MaxValueValidator(2100)]),
        ),
    ]
