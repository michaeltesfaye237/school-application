# Generated by Django 2.2.5 on 2020-11-22 08:42

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('school', '0006_auto_20201122_1107'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='teachercourse',
            unique_together={('teacher', 'year', 'course')},
        ),
    ]
