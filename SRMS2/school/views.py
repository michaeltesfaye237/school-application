from django.shortcuts import render, redirect
from django.urls import reverse_lazy, reverse
from django.http import  HttpResponse
from django.contrib.auth.decorators import login_required
from users.decorator import teacher_authentication, student_authentication, officer_authentication
from .models import Assessment, TeacherCourse, Mark, StudentSection, Course, Section, Information, Transcript
from .forms import AssessmentCreation, CSVMarkUpload, SectionCreationForm, CourseCreationForm
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
import csv, io
from django.utils import timezone
from django.contrib import messages
from users.models import User
from django.db import IntegrityError
# Create your views here.

def cal_year():
    try:
        try:
            year = int(Information.objects.get(name='academic_year').value)
        except ValueError:
            year = timezone.now().year - 7
    except Information.DoesNotExist:
        year = timezone.now().year - 7
    return year


@login_required(login_url='login')
@teacher_authentication
def view_assessment(request):
    year = cal_year()
    teacher = request.user
    assessment = Assessment.objects.filter(teacher_course__teacher = teacher, teacher_course__year=year)
    return render(request, "view_assessment.html", {'assessment': assessment})


@login_required(login_url='login')
@teacher_authentication
def create_assessment(request):
    year = cal_year()
    course_list = TeacherCourse.objects.filter(teacher=request.user, year=year)
    error = None
    if request.method == "POST":
        form = AssessmentCreation(request.POST)
        form.fields['teacher_course'].queryset = course_list
        if form.is_valid():
            try:
                post = form.save(commit=False)
                post.save()
                return redirect('view_assessment')
            except Exception:
                error = 'Total assessment should not exceed 100.'
    else:
        form = AssessmentCreation()
        form.fields['teacher_course'].queryset = course_list
    return render(request, "create_assessment.html", {"form": form, 'error': error})



@login_required(login_url='login')
@teacher_authentication
def class_taught_list(request):
    year = cal_year()
    course_list = TeacherCourse.objects.filter(teacher=request.user, year=year)
    
    return render(request, "class_taught_list.html", {"course_list": course_list})



@login_required(login_url='login')
@teacher_authentication
def student_mark_list(request, course_id):
    year = cal_year()
    course_teacher = TeacherCourse.objects.get(id=course_id)
    course = course_teacher.course
    student_list = StudentSection.objects.filter(section = course.section)
    first_assessment_list = Assessment.objects.filter(teacher_course=course_teacher, teacher_course__year=year, semester="I")
    first_semester_student_mark_list = []
    for stu in student_list:
        mark = Mark.objects.filter(student=stu, course=course, assessment__semester="I")
        
        ass_list = {ass:'' for ass in first_assessment_list}
        for m in mark:
            ass_list[m.assessment] = m.point
        first_semester_student_mark_list.append((stu, ass_list.values()))

    second_assessment_list = Assessment.objects.filter(teacher_course=course_teacher, semester="II")
    second_semester_student_mark_list = []
    for stu in student_list:
        mark = Mark.objects.filter(student=stu, course=course, assessment__semester="II")
        
        ass_list = {ass:'' for ass in second_assessment_list}
        for m in mark:
            ass_list[m.assessment] = m.point
        second_semester_student_mark_list.append((stu, ass_list.values()))


    return render(request, "student_mark_list.html", 
                    {
                        'first_semester_student_mark_list': first_semester_student_mark_list,
                        "second_semester_student_mark_list": second_semester_student_mark_list,
                        'first_assessment_list': first_assessment_list,
                        'second_assessment_list': second_assessment_list,
                        'course_teacher': course_teacher
                    }
                )




@login_required(login_url='login')
@teacher_authentication
def insert_mark(request, student_section_id, course_teacher_id, semester):
    year = cal_year()
    course_teacher = TeacherCourse.objects.get(id=course_teacher_id)
    assessment_list = Assessment.objects.filter(teacher_course=course_teacher, teacher_course__year=year, semester=semester)
    student_section = StudentSection.objects.get(id=student_section_id) 
    mark = Mark.objects.filter(student=student_section, course=course_teacher.course, student__year=year, assessment__semester=semester)
    mark_list = {m.assessment: m.point for m in mark}
    for ass in assessment_list:
        if ass in mark_list:
            ass.value = mark_list[ass]
        else:
            ass.value = ''
    error = None
    if request.method == "POST":
        for assessment in assessment_list:
            point = request.POST[str(assessment.id)]
            
            try:
                prevous_mark = Mark.objects.get(student=student_section, course=course_teacher.course, assessment=assessment)
                if point:
                    prevous_mark.point = point
                    try:
                        prevous_mark.save()
                    except Exception:
                        error = 'for {} the maximum point is {}'.format(prevous_mark.assessment.name, prevous_mark.assessment.point)
                else:
                    prevous_mark.delete()
            except Mark.DoesNotExist:
                if point:
                    mark = Mark(student=student_section, course=course_teacher.course, assessment=assessment, point=point)
                    try:
                        mark.save()
                    except Exception:
                        error = 'for {} the maximum point is {}'.format(mark.assessment.name, mark.assessment.point)
        if not error:
            return redirect('student_mark_list', course_teacher_id)
    return render(request, "insert_mark.html", {'assessment_list': assessment_list, 'student_name': student_section.student, 'error': error})


#def create_section(request):
 #   return render(request, 'section/create_section.html')

def section_upload(request):
    template = "section/section_upload.html"
    data = Section.objects.all()
    context = {
        'sections': data
    }

    if request.method == "GET":
        return render(request, template, context)

    csv_file = request.FILES['file']
    if not csv_file.name.endswith('.csv'):
        messages.error(request, 'This is not a csv file')

    data_set = csv_file.read().decode('UTF-8')
    io_string = io.StringIO(data_set)
    next(io_string)
    for column in csv.reader(io_string, delimiter=',', quotechar="|"):
        _, created = Section.objects.update_or_create(
            grade = column[0],
            name = column[1]
        )
    
    return redirect('list_section')

    
def validate_data(request, user_id, name, mark_list, error):
    if not user_id:
        error.append("there is no ID for student "  + name )
        return 0
    else:
        try:
            User.objects.get(user_id=user_id)
        except User.DoesNotExist:
            error.append("invalid user id for student " + name)
            return 0

    if not name:
        error.append("invalid name for user " + user_id)
        return 0
    for ass in mark_list:
        mark = mark_list[ass]
        if mark and not mark.isdigit():
            error.append("invalid mark on assessment {}  for user {}".format( ass, user_id))
            return 0
    
    return 1

    
def upload_csv(request, csv_file, assessments, course_teacher_id, error):
    
    course = TeacherCourse.objects.get(id=course_teacher_id).course
    if not ( csv_file.content_type == 'text/csv' or csv_file.content_type == 'application/vnd.ms-excel'):
        error.append("The file is not csv format.")
        return redirect('student_mark_list', course_teacher_id)
    else:
        decoded_file = csv_file.read().decode('utf-7').splitlines()
        reader = csv.DictReader(decoded_file)
        line_number = 0
        for row in reader:
            # basic info
            mark_list = {}
            try:
                user_id = row['ID'].strip()
                name = row['name'].strip()
                for ass in assessments:
                    mark_list[ass] = row[ass.name]
            except  KeyError:
                error.append("invalid column header in csv file.Column headers must be contain ID, name, " + ', '.join([ass.name for ass in assessments]))
                return redirect('student_mark_list', course_teacher_id)

            # validate data
            val = validate_data(request, user_id, name, mark_list, error)
            
            if not val:
                continue
            for ass  in mark_list:
                mark = mark_list[ass]
                if mark:
                    student = StudentSection.objects.get(student__user_id=user_id)
                    try:
                        prevous_mark = Mark.objects.get(student=student, course=course, assessment=ass)
                        prevous_mark.point = mark
                        try:
                            prevous_mark.save()
                        except Exception:
                            error.append('invalid result for student {} for {} the maximum point is {}'.format(prevous_mark.student.student, ass.name, ass.point))
                    except Mark.DoesNotExist:
                        try:
                            try:
                                obj, created = Mark.objects.get_or_create(
                                    student=student,
                                    course=course,
                                    assessment=ass,
                                    point=mark,
                                )
                                obj.save()
                            except Exception:
                                error.append('invalid result for student {} for {} the maximum point is {} '.format(student.student, ass.name, ass.point))
                        except IntegrityError:
                            error.append("invalid information for user " + name)
      
    return redirect('student_mark_list', course_teacher_id)


@login_required(login_url='login')
@teacher_authentication
def upload_marks(request, course_teacher_id, semester):
    year = cal_year()
    error = []
    course_teacher = TeacherCourse.objects.get(id=course_teacher_id)
    assessments = [ass for ass in Assessment.objects.filter(teacher_course=course_teacher, teacher_course__year=year, semester=semester)]
    if request.method =="POST":
        form = CSVMarkUpload(request.POST, request.FILES)
        if form.is_valid():
            file = request.FILES.get('file')
            upload_csv(request, file, assessments, course_teacher_id, error)
            if not error:
                return redirect('student_mark_list', course_teacher_id)
    else:
        form = CSVMarkUpload()
    return render(request, 'upload_marks.html', {'form': form, 'error': error})



@login_required(login_url='login')
@teacher_authentication
def grade_report(request, course_teacher_id, semester):
    year = cal_year()
    error = []
    course_teacher = TeacherCourse.objects.get(id=course_teacher_id)
    course = course_teacher.course
    assessments = [ass for ass in Assessment.objects.filter(teacher_course=course_teacher, teacher_course__year=year, semester=semester)]
    student_list = StudentSection.objects.filter(section=course.section, year=year)
    tot = 0
    for ass in assessments:
        tot += ass.point
    if tot != 100:
        error.append( "Total mark should be 100")
        return render(request, 'grade_report.html', {'error': error})
    student_mark_dict = dict()
    for student in student_list:
        total = 0
        for ass in assessments:
            try:
                mark = Mark.objects.get(student=student, assessment=ass, course=course)
            except Mark.DoesNotExist:
                error.append('There is an empty mark on the assessment list')
                return render(request, 'grade_report.html', {'error': error})
            total += mark.point
        student_mark_dict[student] = total
    
    for student in student_mark_dict:
        mark = student_mark_dict[student]
        try:
            transcript = Transcript.objects.get(student=student, course=course, semester=semester)
            transcript.point = mark
            transcript.save()
        except Transcript.DoesNotExist:
            transcript = Transcript(student=student, course=course, point=mark, semester=semester)
            transcript.save()
    return render(request, 'grade_report.html', {'error': error})



@login_required(login_url='login')
@student_authentication
def view_mark(request):
    # student_section = StudentSection.objects.get(student=request.user, year=year)
    first_total_mark = []
    second_total_mark = []

    all_student_section = StudentSection.objects.filter(student=request.user).order_by('year').reverse()
    for student_section in all_student_section:
        # first_total_mark = []
        section = student_section.section
        course = Course.objects.filter(section=section)
        
        for subject in course:
            mark = Mark.objects.filter(student=student_section, course=subject, assessment__semester='I')
            acc = 0
            out_off = 0
            try:
                teacher_course = TeacherCourse.objects.get(course=subject)
            except TeacherCourse.DoesNotExist:
                teacher_course = None
            for each in mark:
                acc += each.point
                out_off += each.assessment.point
            
            first_total_mark.append([mark, teacher_course , acc, out_off])
        
        # second_total_mark = []
        for subject in course:
            mark = Mark.objects.filter(student=student_section, course=subject, assessment__semester='II')
            acc = 0
            out_off = 0
            try:
                teacher_course = TeacherCourse.objects.get(course=subject)
            except TeacherCourse.DoesNotExist:
                teacher_course = None
            for each in mark:
                acc += each.point
                out_off += each.assessment.point
            
            second_total_mark.append([mark, teacher_course, acc, out_off])
        
    return render(request, 'view_mark.html', {'first_total_mark': first_total_mark, 'second_total_mark': second_total_mark})


@login_required(login_url='login')
@student_authentication
def transcript(request):
    first_transcript = []
    second_transcript = []
    average_transcript = []

    all_student_section = StudentSection.objects.filter(student=request.user).order_by('year').reverse()
    for student_section in all_student_section:        
        yearly_report = []
        transcript = Transcript.objects.filter(student=student_section, semester='I')
        total = 0
        for trans in transcript:
            yearly_report.append([trans.course.name, trans.point])
            total += trans.point
        if len(yearly_report):
            average = round(float(total) / len(yearly_report), 2)
        else: average = 0.00
        first_transcript.append([yearly_report, total, average, student_section.section, student_section.year])
    
    for student_section in all_student_section:       
        yearly_report = []
        transcript = Transcript.objects.filter(student=student_section, semester='II')
        total = 0
        for trans in transcript:
            yearly_report.append([trans.course.name, trans.point])
            total += trans.point
        if len(yearly_report):
            average = round(float(total) / len(yearly_report), 2)
        else: average = 0.00
        second_transcript.append([yearly_report, total, average, student_section.section, student_section.year])
    
    for student_section in all_student_section:        
        yearly_report = []
        transcript_1 = {trans.course.name: float(trans.point) for trans in Transcript.objects.filter(student=student_section, semester='I')}
        transcript_2 = {trans.course.name: float(trans.point) for trans in Transcript.objects.filter(student=student_section, semester='II')}
       
        total = 0
        for course in transcript_1:
            if course in transcript_2:
                ave = (transcript_1[course] + transcript_2[course])/2.0
                yearly_report.append([course, ave])
                total += ave
        if len(yearly_report):
            average = round(float(total) / len(yearly_report), 2)
        else: average = 0.00
        average_transcript.append([yearly_report, total, average, student_section.section, student_section.year])
    
    return render(request, 'transcript.html', {'first_transcript': first_transcript, 'second_transcript': second_transcript, 'average_transcript': average_transcript})


class SectionListView(LoginRequiredMixin, ListView):
    model = Section
    template_name = 'section/section_list.html'

class SectionCreateView(LoginRequiredMixin, CreateView):
    form_class = SectionCreationForm
    success_url = reverse_lazy('list_section')
    template_name = 'section/section_create.html'
   
class SectionUpdateView(LoginRequiredMixin, UpdateView):
    model = Section
    fields = ('grade', 'name')
    success_url = reverse_lazy('list_section')
    template_name = 'section/section_edit.html'

class SectionDeleteView(LoginRequiredMixin, DeleteView):
    model = Section
    success_url = reverse_lazy('list_section')
    template_name = 'section/section_delete.html'

class CourseListView(LoginRequiredMixin, ListView):
    model = Course
    template_name = 'course/course_list.html'

class CourseCreateView(LoginRequiredMixin, CreateView):
    form_class = CourseCreationForm
    success_url = reverse_lazy('list_course')
    template_name = 'course/course_create.html'
   
class CourseUpdateView(LoginRequiredMixin, UpdateView):
    model = Course
    fields = ('name')
    success_url = reverse_lazy('list_course')
    template_name = 'course/course_edit.html'

class CourseDeleteView(LoginRequiredMixin, DeleteView):
    model = Course
    success_url = reverse_lazy('list_course')
    template_name = 'course/course_delete.html'

class AssessmentUpdateView(LoginRequiredMixin, UpdateView):
    model = Assessment
    fields = ('__all__')
    success_url = reverse_lazy('view_assessment')
    template_name = 'assessment_edit.html'

class AssessmentDeleteView(LoginRequiredMixin, DeleteView):
    model = Assessment
    success_url = reverse_lazy('view_assessment')
    template_name = 'assessment_delete.html'