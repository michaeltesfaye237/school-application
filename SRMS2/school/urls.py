from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('view-assessment/', views.view_assessment, name='view_assessment'),
    path('create-assessment/', views.create_assessment, name='create_assessment'),
    path('class-taught-list/', views.class_taught_list, name='class_taught_list'),
    path('student-mark-list/<int:course_id>/', views.student_mark_list, name='student_mark_list'),
    path('insert-mark/<int:student_section_id>/<int:course_teacher_id>/<semester>/', views.insert_mark, name='insert_mark'),
    path('upload-marks/<int:course_teacher_id>/<semester>/', views.upload_marks, name='upload_marks'),
    path('view-mark/', views.view_mark, name='view_mark'),
    path('grdae-report/<int:course_teacher_id>/<semester>/', views.grade_report, name='grade_report'),
    path('transcript/', views.transcript, name='transcript'),

    path('sections/list/', views.SectionListView.as_view(), name='list_section'),
    path('sections/upload/', views.section_upload, name='section_upload'),
    path('sections/create/', views.SectionCreateView.as_view(), name='create_section'),
    path('sections/<int:pk>/edit/', views.SectionUpdateView.as_view(), name='section_edit'),
    path('sections/<int:pk>/delete/', views.SectionDeleteView.as_view(), name='section_delete'),

    path('courses/list/', views.CourseListView.as_view(), name='list_course'),
    #path('sections/upload/', views.section_upload, name='section_upload'),
    path('courses/create/', views.CourseCreateView.as_view(), name='create_course'),
    path('courses/<int:pk>/edit/', views.CourseUpdateView.as_view(), name='course_edit'),
    path('courses/<int:pk>/delete/', views.CourseDeleteView.as_view(), name='course_delete'),

    path('assessment/assessment_edit/<int:pk>/', views.AssessmentUpdateView.as_view(), name='assessment_edit'),
    path('assessment/assessment_delete/<int:pk>/', views.AssessmentDeleteView.as_view(), name='assessment_delete'),
]

