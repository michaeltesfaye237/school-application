from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Course)
admin.site.register(Section)
admin.site.register(TeacherCourse)
admin.site.register(StudentSection)
admin.site.register(Assessment)
admin.site.register(Mark)
admin.site.register(Information)
admin.site.register(Transcript)