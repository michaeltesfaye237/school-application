from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from users.models import User
from django.utils import timezone

choice = ((chr(i),chr(i)) for i in range(65, 91))
class Section(models.Model):
    grade = models.PositiveIntegerField(validators=[ MaxValueValidator(12)])
    name = models.CharField(choices=choice, max_length=50)

    def __str__(self):
        return str(self.grade) + self.name


class Course(models.Model):
    section = models.ForeignKey(Section, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)

    def __str__(self):
        return "{} {}".format(self.section, self.name)


class TeacherCourse(models.Model):
    teacher = models.ForeignKey(User, on_delete=models.CASCADE, limit_choices_to={"role": "teacher"})
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    year = models.PositiveIntegerField(validators=[MinValueValidator(2010), MaxValueValidator(2100)])

    def __str__(self):
        return "{} by {} on {}".format(self.course, self.teacher, self.year)
    class Meta:
        unique_together = ('year', 'course')
       


class StudentSection(models.Model):
    student = models.ForeignKey(User, on_delete=models.CASCADE, limit_choices_to={"role": "student"})
    section = models.ForeignKey(Section, on_delete=models.CASCADE)
    year = models.PositiveIntegerField(validators=[MinValueValidator(2010), MaxValueValidator(2100)], default=timezone.now().year - 7)

    class Meta:
        unique_together = ('student', 'year')

    def __str__(self):
        return "{} {} on {}".format(self.student, self.section, self.year)


class Assessment(models.Model):
    name = models.CharField(max_length=50)
    point = models.DecimalField(decimal_places = 2, max_digits =5, validators=[MinValueValidator(0), MaxValueValidator(100)])
    teacher_course = models.ForeignKey(TeacherCourse, on_delete=models.CASCADE)
    semester = models.CharField(max_length=200, choices=(('I', 'I'), ('II', 'II')))

    def __str__(self):
        return "{} on course {}".format(self.name, self.teacher_course.course)

    def save(self, *args, **kwargs):
        assessments = Assessment.objects.filter(teacher_course=self.teacher_course, semester=self.semester)
        total = self.point
        for ass in assessments:
            total += ass.point
        if total > 100:
            raise Exception('Total assessment should not exceed 100.')
        super(Assessment, self).save(*args, **kwargs)


class Mark(models.Model):
    student = models.ForeignKey(StudentSection, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    assessment = models.ForeignKey(Assessment, on_delete=models.CASCADE)
    point = models.DecimalField(decimal_places = 2, max_digits =5, validators=[MinValueValidator(0), MaxValueValidator(100)])

    def __str__(self):
        return "{}  {}".format(self.student, self.assessment)   

    def save(self, *args, **kwargs):
        # print(self.point , self.assessment.point, float(self.point) > float(self.assessment.point))
        if float(self.point) > float(self.assessment.point):
            raise Exception('for {} the maximum point is {}'.format(self.assessment.name, self.assessment.point))
        super(Mark, self).save(*args, **kwargs)


class Information(models.Model):
    name = models.CharField(max_length=50)
    value =  models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Transcript(models.Model):
    student = models.ForeignKey(StudentSection, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    semester = models.CharField(max_length=200, choices=(('I', 'I'), ('II', 'II')))
    point = models.DecimalField(decimal_places = 2, max_digits =5, validators=[MinValueValidator(0), MaxValueValidator(100)])

    class Meta:
        unique_together = ('student', 'course', 'semester')

    def __str__(self):
        return "{}  {} on semester {}".format(self.student, self.course, self.semester)   

    def save(self, *args, **kwargs):
        if self.course.section != self.student.section:
            raise Exception('Student Section and Course section must have the same')
        super(Transcript, self).save(*args, **kwargs)

