from django import forms
from .models import Assessment, Section, Course
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column
class AssessmentCreation(forms.ModelForm):
    class Meta:
        model = Assessment
        fields = ('name', 'point', 'teacher_course', 'semester')

class CSVMarkUpload(forms.Form):
    file = forms.FileField(
        label='csv',
        widget=forms.FileInput(),
        help_text='* choose csv file.'

    )

class SectionCreationForm(forms.ModelForm):
    class Meta:
        model = Section
        fields = ('grade', 'name')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Column('grade', css_class='col-md-5 offset-sm-1 mb-0'),
            Column('name', css_class='col-md-5 offset-sm-1 mb-0'),
            Submit('submit', 'Create', css_class='offset-sm-1 mb-0')
        )

class CourseCreationForm(forms.ModelForm):
    class Meta:
        model = Course
        fields = ('section', 'name')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Column('section', css_class='col-md-5 offset-sm-1 mb-0'),
            Column('name', css_class='col-md-5 offset-sm-1 mb-0'),
            Submit('submit', 'Create', css_class='offset-sm-1 mb-0')
        )